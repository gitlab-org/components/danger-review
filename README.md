# `danger-review` component

The component generates a `danger-review` job which posts an automated comment after running the Danger rules from the project.

## Usage

To utilize this component, add it to an existing `.gitlab-ci.yml` file using the `include:` keyword:

```yaml
include:
  - component: gitlab.com/gitlab-org/components/danger-review/danger-review@~latest
  # or a specific version
  - component: gitlab.com/gitlab-org/components/danger-review/danger-review@<VERSION>
```

Create a [masked](https://docs.gitlab.com/ee/ci/variables/#mask-a-cicd-variable) CI/CD
variable named `DANGER_GITLAB_API_TOKEN` that contains a project access token
with `api` scope and `Developer` role.

## Inputs

| Input                             | Default Value                 | Description                                                                                       |
| --------------------------------- | ----------------------------- | ------------------------------------------------------------------------------------------------- |
| `job_image`                       | `ruby:3.3`                    | The image to use for the `danger-review` job.                                                     |
| `job_stage`                       | `test`                        | CI Pipeline stage where danger-review job should run.                                             |
| `job_allow_failure`               | `false`                       | Whether or not the job is allowed to fail.                                                        |
| `gitlab_dangerfiles_version`      | Empty                         | Version requirement for `gitlab-dangerfiles`.                                                     |
| `dangerfile`                      | `Dangerfile`                  | Location of the Dangerfile.                                                                       |
| `dry_run`                         | `false`                       | Run Danger in "dry run" mode with local only plugin inside a Dangerfile.                          |

### Example

* `.gitlab-ci.yml`

```yaml
include:
  - component: "gitlab.com/gitlab-org/components/danger-review/danger-review@~latest"
    inputs:
      dangerfile: "custom/Dangerfile"
```

* `Dangerfile`

```ruby
# frozen_string_literal: true

require 'gitlab-dangerfiles'

Gitlab::Dangerfiles.for_project(self, &:import_defaults)
```

## Contribute

Please read about CI/CD components and best practices at: https://docs.gitlab.com/ee/ci/components
